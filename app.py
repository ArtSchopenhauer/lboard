from flask import Flask, render_template, request
import json
import requests
import datetime
import pytz
import cPickle as pickle

app = Flask(__name__)


# returns rendered home view with ambassador stats table
@app.route("/")
def home_view():
	pickled_data_file = open("/root/lboard/picklejar/data.pkl")
	full_data = pickle.load(pickled_data_file) 
	amb_stats_suffolk = full_data["amb_stats_suffolk"]
	amb_stats_nassau = full_data["amb_stats_nassau"]
	salestw_suffolk = full_data["salestw_suffolk"]
	salestw_nassau = full_data["salestw_nassau"]
	return render_template("data.html", 
					       amb_stats_nassau=amb_stats_nassau,
						   amb_stats_suffolk=amb_stats_suffolk,
						   salestw_suffolk=salestw_suffolk,
						   salestw_nassau=salestw_nassau)
	pickled_data_file.close()

@app.route("/lastweek")
def last_week_view():
	pickled_data_file = open("/root/lboard/picklejar/datalw.pkl")
	full_data = pickle.load(pickled_data_file) 
	amb_stats_suffolk = full_data["amb_stats_suffolk"]
	amb_stats_nassau = full_data["amb_stats_nassau"]
	salestw_suffolk = full_data["salestw_suffolk"]
	salestw_nassau = full_data["salestw_nassau"]
	return render_template("datalw.html", 
					       amb_stats_nassau=amb_stats_nassau,
						   amb_stats_suffolk=amb_stats_suffolk,
						   salestw_suffolk=salestw_suffolk,
						   salestw_nassau=salestw_nassau)
	pickled_data_file.close()

@app.route("/pods")
def overview():
	pickled_data_file = open("/root/lboard/picklejar/overview.pkl")
	info = pickle.load(pickled_data_file) 
	return render_template("overview.html", info=info)
	pickled_data_file.close()

@app.route("/pods/nassau")
def nassau_pods():
	pickled_data_file = open("/root/lboard/picklejar/pods.pkl")
	info = pickle.load(pickled_data_file) 
	return render_template("nassau_pods.html", info=info)
	pickled_data_file.close()

@app.route("/pods/suffolk")
def suffolk_pods():
	pickled_data_file = open("/root/lboard/picklejar/pods.pkl")
	info = pickle.load(pickled_data_file) 
	return render_template("suffolk_pods.html", info=info)
	pickled_data_file.close()

@app.route("/outlook")
def outlook():
	pickled_dates_file = open("/root/lboard/picklejar/dates.pkl")
	dates = pickle.load(pickled_dates_file)
	pickled_data_file = open("/root/lboard/picklejar/outlook.pkl")
	appts = pickle.load(pickled_data_file)
	return render_template("outlook.html",
						   appts=appts,
						   dates=dates)


if __name__ == "__main__":
	app.run(debug=True)
