import json
import cPickle as pickle

pickled_data_file = open("/root/lboard/picklejar/pods.pkl")
info = pickle.load(pickled_data_file)
pickled_data_file.close()

overview = {"nassau": {"sets_today": 0, "sits_today": 0, "sales_today": 0, "sets_tw": 0, "sits_tw": 0, "sales_tw": 0},
			"suffolk": {"sets_today": 0, "sits_today": 0, "sales_today": 0, "sets_tw": 0, "sits_tw": 0, "sales_tw": 0},
			"total": {"sets_today": 0, "sits_today": 0, "sales_today": 0, "sets_tw": 0, "sits_tw": 0, "sales_tw": 0}}

overview["nassau"]["sets_today"] = info["n1"]["totals"]["sets_today"] + info["n2"]["totals"]["sets_today"]
overview["nassau"]["sits_today"] = info["n1"]["totals"]["sits_today"] + info["n2"]["totals"]["sits_today"]
overview["nassau"]["sales_today"] = info["n1"]["totals"]["sales_today"] + info["n2"]["totals"]["sales_today"]
overview["nassau"]["sets_tw"] = info["n1"]["totals"]["sets_tw"] + info["n2"]["totals"]["sets_tw"]
overview["nassau"]["sits_tw"] = info["n1"]["totals"]["sits_tw"] + info["n2"]["totals"]["sits_tw"]
overview["nassau"]["sales_tw"] = info["n1"]["totals"]["sales_tw"] + info["n2"]["totals"]["sales_tw"]
overview["suffolk"]["sets_today"] = info["s1"]["totals"]["sets_today"] + info["s2"]["totals"]["sets_today"] + info["s3"]["totals"]["sets_today"]
overview["suffolk"]["sits_today"] = info["s1"]["totals"]["sits_today"] + info["s2"]["totals"]["sits_today"] + info["s3"]["totals"]["sits_today"]
overview["suffolk"]["sales_today"] = info["s1"]["totals"]["sales_today"] + info["s2"]["totals"]["sales_today"] + info["s3"]["totals"]["sales_today"]
overview["suffolk"]["sets_tw"] = info["s1"]["totals"]["sets_tw"] + info["s2"]["totals"]["sets_tw"] + info["s3"]["totals"]["sets_tw"]
overview["suffolk"]["sits_tw"] = info["s1"]["totals"]["sits_tw"] + info["s2"]["totals"]["sits_tw"] + info["s3"]["totals"]["sits_tw"]
overview["suffolk"]["sales_tw"] = info["s1"]["totals"]["sales_tw"] + info["s2"]["totals"]["sales_tw"] + info["s3"]["totals"]["sales_tw"]
overview["total"]["sets_today"] = overview["nassau"]["sets_today"] + overview["suffolk"]["sets_today"]
overview["total"]["sits_today"] = overview["nassau"]["sits_today"] + overview["suffolk"]["sits_today"]
overview["total"]["sales_today"] = overview["nassau"]["sales_today"] + overview["suffolk"]["sales_today"]
overview["total"]["sets_tw"] = overview["nassau"]["sets_tw"] + overview["suffolk"]["sets_tw"]
overview["total"]["sits_tw"] = overview["nassau"]["sits_tw"] + overview["suffolk"]["sits_tw"]
overview["total"]["sales_tw"] = overview["nassau"]["sales_tw"] + overview["suffolk"]["sales_tw"]

overview_pickle_file = open("/root/lboard/picklejar/overview.pkl", "wb")
pickle.dump(overview, overview_pickle_file)
overview_pickle_file.close()