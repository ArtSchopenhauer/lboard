from flask import Flask, render_template, request
import json
import requests
import datetime
import pytz
import cPickle as pickle

# API endpoints
leads = "https://levelsolar.secure.force.com/api/services/apexrest/leads"
contacts = "https://levelsolar.secure.force.com/api/services/apexrest/contacts"
interactions = "https://levelsolar.secure.force.com/api/services/apexrest/interactions"

ambassadors = [{"name": "Bradley Bell", "id": "a027000000Q86bl", "county": "Nassau", "salescorrection": 4, "sitscorrection": 17},
			   {"name": "Casey O'Brien", "id": "a027000000Q86c5", "county": "Nassau", "salescorrection": 15, "sitscorrection": 29},
			   {"name": "Andrew Field", "id": "a027000000Rlv53", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Lonnie Edwards", "id": "a027000000Qqn7u", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Ryan Samida", "id": "a027000000RvoO8", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Doug Stevens", "id": "a027000000RwlSZ", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Mac Smith", "id": "a027000000Rlv4o", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Nestor Colon", "id": "a027000000PxIOQ", "county": "Suffolk", "salescorrection": 23, "sitscorrection": 42},
			   {"name": "Carlos Vega", "id": "a027000000R3JB2", "county": "Suffolk", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Mike Desiderio", "id": "a027000000QsUeE", "county": "Suffolk", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Francis D'Erasmo", "id": "a027000000Px6vS", "county": "Suffolk", "salescorrection": 17, "sitscorrection": 52},
			   {"name": "Mike Russo", "id": "a027000000Pue1Z", "county": "Suffolk", "salescorrection": 20, "sitscorrection": 37},
			   {"name": "Anthony Quezada", "id": "a027000000Pue0o", "county": "Suffolk", "salescorrection": 37, "sitscorrection": 70},
			   {"name": "Victor Borisov", "id": "a027000000RyOFN", "county": "Suffolk", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Robert Dees", "id": "a027000000RvoZ7", "county": "Suffolk", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Jeremy Weissman", "id": "a027000000PXjka", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Raymond Armstrong", "id": "a027000000SKN4R", "county": "Suffolk", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Juliana Eckel", "id": "a027000000SbnHW", "county": "Suffolk", "salescorrection": 0, "sitscorrection": 0}, 
			   {"name": "Andrew Malca", "id": "a027000000SHtTj", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Jamal Sealy", "id": "a027000000PYkpB", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Brenda Larsen", "id": "a027000000SKNZq", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Caitlyn Weiss", "id": "a027000000SeQfE", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Dorothy Pitti", "id": "a027000000SgtbB", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Phil Hanks", "id": "a027000000Sgteb", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Andrew Drewchin", "id": "a027000000SeRyO", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0}]

pods = {"n1": [{"name": "Mac Smith (L)", "id": "a027000000Rlv4o"},
			   {"name": "Ryan Samida", "id": "a027000000RvoO8"},
			   {"name": "Brenda Larsen", "id": "a027000000SKNZq"},
			   {"name": "Caitlyn Weiss", "id": "a027000000SeQfE"},
			   {"name": "Jamal Sealy", "id": "a027000000PYkpB"},
			   {"name": "Lonnie Edwards", "id": "a027000000Qqn7u"},
			   {"name": "Andrew Drewchin", "id": "a027000000SeRyO"}], 
		"n2": [{"name": "Casey O'Brien (L)", "id": "a027000000Q86c5"},
			   {"name": "Andrew Field", "id": "a027000000Rlv53"},
			   {"name": "Jeremy Weissman", "id": "a027000000PXjka"},
			   {"name": "Andrew Malca", "id": "a027000000SHtTj"},
			   {"name": "Dorothy Pitti", "id": "a027000000SgtbB"},
			   {"name": "Phil Hanks", "id": "a027000000Sgteb"}],
		"s1": [{"name": "Nestor Colon (L)", "id": "a027000000PxIOQ"},
			   {"name": "Carlos Vega", "id": "a027000000R3JB2"},
			   {"name": "Robert Dees", "id": "a027000000RvoZ7"}],
		"s2": [{"name": "Francis D'Erasmo (L)", "id": "a027000000Px6vS"},
			   {"name": "Mike Russo", "id": "a027000000Pue1Z"},
			   {"name": "Juliana Eckel", "id": "a027000000SbnHW"}],
		"s3": [{"name": "Michael Desiderio (L)", "id": "a027000000QsUeE"},
			   {"name": "Raymond Armstrong", "id": "a027000000SKN4R"},
			   {"name": "Victor Borisov", "id": "a027000000RyOFN"}]}

# time definitions
utc_zone = pytz.timezone('UTC')
est_zone = pytz.timezone('US/Eastern')
now_utc_naive = datetime.datetime.utcnow()
now_utc_aware = utc_zone.localize(now_utc_naive)
now_est_aware = now_utc_aware.astimezone(est_zone)
today_12am_est = now_est_aware.replace(hour=0, minute=0, second=0)
today_12am_est_in_utc = today_12am_est.astimezone(utc_zone)
today_12am_est_in_utc_naive = today_12am_est_in_utc.replace(tzinfo=None)
today_12am_iso = today_12am_est_in_utc_naive.isoformat()

# returns Monday 12am EST as naive UTC datetime
def week_start():
	one_day = datetime.timedelta(days=1)
	today_int = now_est_aware.weekday()
	this_time_est_monday = now_est_aware - (one_day * today_int)
	offset = (now_utc_aware.hour - now_est_aware.hour)
	if offset < 0:
		offset = (now_utc_aware.hour + 24) - now_est_aware.hour
	monday_12am_est_aware = this_time_est_monday.replace(hour=offset, minute=0, second=0, microsecond=0)
	monday_12am_est_naive = monday_12am_est_aware.replace(tzinfo=None)
	return monday_12am_est_naive

now_iso = now_utc_naive.isoformat()
week_start_iso = week_start().isoformat()

def get_sets_period(ambassador_id, start):
	full_list = []
	params = {"subject": "Closer Appointment", "createddate__gt": start, "ordering": "createddate"}
	page_list = requests.get(interactions, params=params).json()
	full_list.extend(page_list)
	while len(page_list) > 0:
		new_params = {"subject": "Closer Appointment", "createddate__gt": page_list[-1]["createddate"], "ordering": "createddate"}
		page_list = requests.get(interactions, params=new_params).json()
		full_list.extend(page_list)
	sets = []
	for item in full_list:
		if item["lead"]:
			if item["lead"]["ambassador"]:
				sets.append(item["lead"]["ambassador"]["id"][0:15])
	return sets.count(ambassador_id)

def amb_metric(ambassador, metric):
	param = {"ambassador": ambassador}
	full_leads_list = []
	desired_leads_list = []
	response_object = requests.get(leads, params=param)
	page_list = json.loads(response_object.text)
	full_leads_list.extend(page_list)
	page_num = 2
	new_param = {"ambassador": ambassador, "page": page_num}
	while len(page_list) == 100:
		response_object = requests.get(leads, params=new_param)
		page_list = json.loads(response_object.text)
		full_leads_list.extend(page_list)
		page_num += 1
		new_param = {"ambassador": ambassador, "page": page_num}
	for item in full_leads_list:
		if item["status"][metric] == True:
			desired_leads_list.append(item)
	return desired_leads_list

def amb_metric_period(ambassador, metric, start):
	list_to_date = amb_metric(ambassador, metric)
	ids_to_date = []
	ids_this_period = []
	for item in list_to_date:
		ids_to_date.append(item["id"])
	param = {"subject": "Closer Appointment",
			 "ambassador": ambassador, 
			 "scheduled_date__gte": start, 
			 "scheduled_date__lte": now_iso}
	response_object = requests.get(interactions, params=param)
	interactions_this_period = json.loads(response_object.text)
	for item in interactions_this_period:
		if item["lead"]["id"] in ids_to_date:
			ids_this_period.append(item["lead"]["id"])
	return len(ids_this_period)

def build_json():
	info = {"n1": {"ambassadors": [], "totals": {"sets_today": 0, "sits_today": 0, "sales_today": 0, "sets_tw": 0, "sits_tw": 0, "sales_tw": 0}},
			"n2": {"ambassadors": [], "totals": {"sets_today": 0, "sits_today": 0, "sales_today": 0, "sets_tw": 0, "sits_tw": 0, "sales_tw": 0}},
			"s1": {"ambassadors": [], "totals": {"sets_today": 0, "sits_today": 0, "sales_today": 0, "sets_tw": 0, "sits_tw": 0, "sales_tw": 0}},
			"s2": {"ambassadors": [], "totals": {"sets_today": 0, "sits_today": 0, "sales_today": 0, "sets_tw": 0, "sits_tw": 0, "sales_tw": 0}},
			"s3": {"ambassadors": [], "totals": {"sets_today": 0, "sits_today": 0, "sales_today": 0, "sets_tw": 0, "sits_tw": 0, "sales_tw": 0}}}
	for item in pods["n1"]:
		stats = {"name": item["name"],
				 "sets_today": get_sets_period(item["id"], today_12am_iso),
				 "sits_today": amb_metric_period(item["id"], "appointment_sit", today_12am_iso),
				 "sales_today": amb_metric_period(item["id"], "sale", today_12am_iso),
				 "sets_tw": get_sets_period(item["id"], week_start_iso),
				 "sits_tw": amb_metric_period(item["id"], "appointment_sit", week_start_iso),
				 "sales_tw": amb_metric_period(item["id"], "sale", week_start_iso)}
		info["n1"]["ambassadors"].append(stats)
		info["n1"]["totals"]["sets_today"] = info["n1"]["totals"]["sets_today"] + stats["sets_today"]
		info["n1"]["totals"]["sits_today"] = info["n1"]["totals"]["sits_today"] + stats["sits_today"]
		info["n1"]["totals"]["sales_today"] = info["n1"]["totals"]["sales_today"] + stats["sales_today"]
		info["n1"]["totals"]["sets_tw"] = info["n1"]["totals"]["sets_tw"] + stats["sets_tw"]
		info["n1"]["totals"]["sits_tw"] = info["n1"]["totals"]["sits_tw"] + stats["sits_tw"]
		info["n1"]["totals"]["sales_tw"] = info["n1"]["totals"]["sales_tw"] + stats["sales_tw"]
	for item in pods["n2"]:
		stats = {"name": item["name"],
				 "sets_today": get_sets_period(item["id"], today_12am_iso),
				 "sits_today": amb_metric_period(item["id"], "appointment_sit", today_12am_iso),
				 "sales_today": amb_metric_period(item["id"], "sale", today_12am_iso),
				 "sets_tw": get_sets_period(item["id"], week_start_iso),
				 "sits_tw": amb_metric_period(item["id"], "appointment_sit", week_start_iso),
				 "sales_tw": amb_metric_period(item["id"], "sale", week_start_iso)}
		info["n2"]["ambassadors"].append(stats)
		info["n2"]["totals"]["sets_today"] = info["n2"]["totals"]["sets_today"] + stats["sets_today"]
		info["n2"]["totals"]["sits_today"] = info["n2"]["totals"]["sits_today"] + stats["sits_today"]
		info["n2"]["totals"]["sales_today"] = info["n2"]["totals"]["sales_today"] + stats["sales_today"]
		info["n2"]["totals"]["sets_tw"] = info["n2"]["totals"]["sets_tw"] + stats["sets_tw"]
		info["n2"]["totals"]["sits_tw"] = info["n2"]["totals"]["sits_tw"] + stats["sits_tw"]
		info["n2"]["totals"]["sales_tw"] = info["n2"]["totals"]["sales_tw"] + stats["sales_tw"]
	for item in pods["s1"]:
		stats = {"name": item["name"],
				 "sets_today": get_sets_period(item["id"], today_12am_iso),
				 "sits_today": amb_metric_period(item["id"], "appointment_sit", today_12am_iso),
				 "sales_today": amb_metric_period(item["id"], "sale", today_12am_iso),
				 "sets_tw": get_sets_period(item["id"], week_start_iso),
				 "sits_tw": amb_metric_period(item["id"], "appointment_sit", week_start_iso),
				 "sales_tw": amb_metric_period(item["id"], "sale", week_start_iso)}
		info["s1"]["ambassadors"].append(stats)
		info["s1"]["totals"]["sets_today"] = info["s1"]["totals"]["sets_today"] + stats["sets_today"]
		info["s1"]["totals"]["sits_today"] = info["s1"]["totals"]["sits_today"] + stats["sits_today"]
		info["s1"]["totals"]["sales_today"] = info["s1"]["totals"]["sales_today"] + stats["sales_today"]
		info["s1"]["totals"]["sets_tw"] = info["s1"]["totals"]["sets_tw"] + stats["sets_tw"]
		info["s1"]["totals"]["sits_tw"] = info["s1"]["totals"]["sits_tw"] + stats["sits_tw"]
		info["s1"]["totals"]["sales_tw"] = info["s1"]["totals"]["sales_tw"] + stats["sales_tw"]
	for item in pods["s2"]:
		stats = {"name": item["name"],
				 "sets_today": get_sets_period(item["id"], today_12am_iso),
				 "sits_today": amb_metric_period(item["id"], "appointment_sit", today_12am_iso),
				 "sales_today": amb_metric_period(item["id"], "sale", today_12am_iso),
				 "sets_tw": get_sets_period(item["id"], week_start_iso),
				 "sits_tw": amb_metric_period(item["id"], "appointment_sit", week_start_iso),
				 "sales_tw": amb_metric_period(item["id"], "sale", week_start_iso)}
		info["s2"]["ambassadors"].append(stats)
		info["s2"]["totals"]["sets_today"] = info["s2"]["totals"]["sets_today"] + stats["sets_today"]
		info["s2"]["totals"]["sits_today"] = info["s2"]["totals"]["sits_today"] + stats["sits_today"]
		info["s2"]["totals"]["sales_today"] = info["s2"]["totals"]["sales_today"] + stats["sales_today"]
		info["s2"]["totals"]["sets_tw"] = info["s2"]["totals"]["sets_tw"] + stats["sets_tw"]
		info["s2"]["totals"]["sits_tw"] = info["s2"]["totals"]["sits_tw"] + stats["sits_tw"]
		info["s2"]["totals"]["sales_tw"] = info["s2"]["totals"]["sales_tw"] + stats["sales_tw"]
	for item in pods["s3"]:
		stats = {"name": item["name"],
				 "sets_today": get_sets_period(item["id"], today_12am_iso),
				 "sits_today": amb_metric_period(item["id"], "appointment_sit", today_12am_iso),
				 "sales_today": amb_metric_period(item["id"], "sale", today_12am_iso),
				 "sets_tw": get_sets_period(item["id"], week_start_iso),
				 "sits_tw": amb_metric_period(item["id"], "appointment_sit", week_start_iso),
				 "sales_tw": amb_metric_period(item["id"], "sale", week_start_iso)}
		info["s3"]["ambassadors"].append(stats)
		info["s3"]["totals"]["sets_today"] = info["s3"]["totals"]["sets_today"] + stats["sets_today"]
		info["s3"]["totals"]["sits_today"] = info["s3"]["totals"]["sits_today"] + stats["sits_today"]
		info["s3"]["totals"]["sales_today"] = info["s3"]["totals"]["sales_today"] + stats["sales_today"]
		info["s3"]["totals"]["sets_tw"] = info["s3"]["totals"]["sets_tw"] + stats["sets_tw"]
		info["s3"]["totals"]["sits_tw"] = info["s3"]["totals"]["sits_tw"] + stats["sits_tw"]
		info["s3"]["totals"]["sales_tw"] = info["s3"]["totals"]["sales_tw"] + stats["sales_tw"]
	pods_file = open("/root/lboard/picklejar/pods.pkl", "wb")
	pickle.dump(info, pods_file)
	pods_file.close()

build_json()