from flask import Flask, render_template, request
import json
import requests
import datetime
import pytz
import cPickle as pickle


pickled_data_file = open("/root/lboard/picklejar/data.pkl")
full_data = pickle.load(pickled_data_file)
pickled_data_file.close()


pickled_data_lw_file = open("/root/lboard/picklejar/datalw.pkl", "wb")
pickle.dump(full_data, pickled_data_lw_file)
pickled_data_lw_file.close()


