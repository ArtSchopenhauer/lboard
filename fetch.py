from flask import Flask, render_template, request
import json
import requests
import datetime
import pytz
import cPickle as pickle

# API endpoints
leads = "https://levelsolar.secure.force.com/api/services/apexrest/leads"
contacts = "https://levelsolar.secure.force.com/api/services/apexrest/contacts"
interactions = "https://levelsolar.secure.force.com/api/services/apexrest/interactions"

# used to construct amb_dict, along with metrics returned by API calls
# corrections account for data pre SFDC implementation
ambassadors = [{"name": "Bradley Bell", "id": "a027000000Q86bl", "county": "Nassau", "salescorrection": 4, "sitscorrection": 17},
			   {"name": "Casey O'Brien", "id": "a027000000Q86c5", "county": "Nassau", "salescorrection": 15, "sitscorrection": 29},
			   {"name": "Andrew Field", "id": "a027000000Rlv53", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Lonnie Edwards", "id": "a027000000Qqn7u", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Ryan Samida", "id": "a027000000RvoO8", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Doug Stevens", "id": "a027000000RwlSZ", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Mac Smith", "id": "a027000000Rlv4o", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Nestor Colon", "id": "a027000000PxIOQ", "county": "Suffolk", "salescorrection": 23, "sitscorrection": 42},
			   {"name": "Carlos Vega", "id": "a027000000R3JB2", "county": "Suffolk", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Mike Desiderio", "id": "a027000000QsUeE", "county": "Suffolk", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Francis D'Erasmo", "id": "a027000000Px6vS", "county": "Suffolk", "salescorrection": 17, "sitscorrection": 52},
			   {"name": "Mike Russo", "id": "a027000000Pue1Z", "county": "Suffolk", "salescorrection": 20, "sitscorrection": 37},
			   {"name": "Anthony Quezada", "id": "a027000000Pue0o", "county": "Suffolk", "salescorrection": 37, "sitscorrection": 70},
			   {"name": "Victor Borisov", "id": "a027000000RyOFN", "county": "Suffolk", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Robert Dees", "id": "a027000000RvoZ7", "county": "Suffolk", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Jeremy Weissman", "id": "a027000000PXjka", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Raymond Armstrong", "id": "a027000000SKN4R", "county": "Suffolk", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Juliana Eckel", "id": "a027000000SbnHW", "county": "Suffolk", "salescorrection": 0, "sitscorrection": 0}, 
			   {"name": "Andrew Malca", "id": "a027000000SHtTj", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Jamal Sealy", "id": "a027000000PYkpB", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Brenda Larsen", "id": "a027000000SKNZq", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Caitlyn Weiss", "id": "a027000000SeQfE", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Dorothy Pitti", "id": "a027000000SgtbB", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Phil Hanks", "id": "a027000000Sgteb", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0},
			   {"name": "Andrew Drewchin", "id": "a027000000SeRyO", "county": "Nassau", "salescorrection": 0, "sitscorrection": 0}]

# used as function parameters to query API
kunzler = "a027000000RHoIZ"
smith = "a027000000Rlv4o"
bell = "a027000000Q86bl"
obrien = "a027000000Q86c5"
field = "a027000000Rlv53"
edwards = "a027000000Qqn7u"
joseph = "a027000000Pue18"
samida = "a027000000RvoO8"
williams = "a027000000RwlSU"
stevens = "a027000000RwlSZ"
colon = "a027000000PxIOQ"
desiderio = "a027000000QsUeE"
derasmo = "a027000000Px6vS"
russo = "a027000000Pue1Z"
quezada = "a027000000Pue0o"
borisov = "a027000000RyOFN"
dees = "a027000000RvoZ7"
luning = "a027000000PxIOa"
vega = "a027000000R3JB2"


# returns list of leads to date for given ambassador with given metric == True
def amb_metric(ambassador, metric):
	param = {"ambassador": ambassador}
	full_leads_list = []
	desired_leads_list = []
	response_object = requests.get(leads, params=param)
	page_list = json.loads(response_object.text)
	full_leads_list.extend(page_list)
	page_num = 2
	new_param = {"ambassador": ambassador, "page": page_num}
	while len(page_list) == 100:
		response_object = requests.get(leads, params=new_param)
		page_list = json.loads(response_object.text)
		full_leads_list.extend(page_list)
		page_num += 1
		new_param = {"ambassador": ambassador, "page": page_num}
	for item in full_leads_list:
		if item["status"][metric] == True:
			desired_leads_list.append(item)
	return desired_leads_list

# time definitions
utc_zone = pytz.timezone('UTC')
est_zone = pytz.timezone('US/Eastern')
now_utc_naive = datetime.datetime.utcnow()
now_utc_aware = utc_zone.localize(now_utc_naive)
now_est_aware = now_utc_aware.astimezone(est_zone)

# returns Monday 12am EST as naive UTC datetime
def week_start():
	one_day = datetime.timedelta(days=1)
	today_int = now_est_aware.weekday()
	this_time_est_monday = now_est_aware - (one_day * today_int)
	offset = (now_utc_aware.hour - now_est_aware.hour)
	if offset < 0:
		offset = (now_utc_aware.hour + 24) - now_est_aware.hour
	monday_12am_est_aware = this_time_est_monday.replace(hour=offset, minute=0, second=0, microsecond=0)
	monday_12am_est_naive = monday_12am_est_aware.replace(tzinfo=None)
	return monday_12am_est_naive

now_iso = now_utc_naive.isoformat()
week_start_iso = week_start().isoformat()

# returns list of lead names for given ambassador (id) with given metric == True AND interaction scheduled date this week;
# depends on amb_metric() and week_start()
def amb_metric_this_week(ambassador, metric):
	list_to_date = amb_metric(ambassador, metric)
	ids_to_date = []
	ids_this_week = []
	for item in list_to_date:
		ids_to_date.append(item["id"])
	param = {"subject": "Closer Appointment",
			 "ambassador": ambassador, 
			 "scheduled_date__gte": week_start_iso, 
			 "scheduled_date__lte": now_iso}
	response_object = requests.get(interactions, params=param)
	interactions_this_week = json.loads(response_object.text)
	for item in interactions_this_week:
		if item["lead"]["id"] in ids_to_date:
			ids_this_week.append(item["lead"]["id"])
	return ids_this_week

# returns stats for all ambassadors as a list of dicts with keys of name, id, sales to date, sales this week, sits to date, sits this week
# depends on ambassadors, amb_metric(), week_start(), and amb_metric_this_week()
def amb_stats(county):
	stats = []
	for amb in ambassadors:
		if amb["county"] == county:
			item = {"name": amb["name"],
					"id": amb["id"],
					"salestd": len(amb_metric(amb["id"],"sale")) + amb["salescorrection"], 
					"salestw": len(amb_metric_this_week(amb["id"],"sale")),
					"sitstd": len(amb_metric(amb["id"],"appointment_sit")) + amb["sitscorrection"],
					"sitstw": len(amb_metric_this_week(amb["id"],"appointment_sit"))}
			stats.append(item)
	return stats

# loads ambassador data from Salesforce into global variable for each county
def load_data():
	global full_data
	global amb_stats_suffolk
	global amb_stats_nassau
	global salestw_suffolk
	global salestw_nassau
	full_data = {}
	salestw_suffolk = 0
	salestw_nassau = 0
	amb_stats_suffolk = amb_stats("Suffolk")
	amb_stats_nassau = amb_stats("Nassau")
	for item in amb_stats_suffolk:
		salestw_suffolk = salestw_suffolk + item["salestw"]
	for item in amb_stats_nassau:
		salestw_nassau = salestw_nassau + item["salestw"]
	full_data = {"amb_stats_suffolk": amb_stats_suffolk,
				 "amb_stats_nassau": amb_stats_nassau,
				 "salestw_suffolk": salestw_suffolk,
				 "salestw_nassau": salestw_nassau}

# loading ambassador data from Salesforce
load_data()

pickled_data_file = open("/root/lboard/picklejar/data.pkl", "wb")
pickle.dump(full_data, pickled_data_file)
pickled_data_file.close()
